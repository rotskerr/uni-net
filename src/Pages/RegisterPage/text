import React, { useState } from "react";
import axios from "axios";
import { FiMail } from "react-icons/fi";
import { RiLockPasswordLine } from "react-icons/ri";
import { Link } from "react-router-dom";

const Login = () => {
  const [data, setData] = useState({
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    const newObj = { ...data, [e.target.name]: e.target.value };
    setData(newObj);
  };

  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      const response = await axios({
        method: "post",
        url: "https://uni-net.fun/api/v1/consumer/login",
        data: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (response.status === 201) {
        console.log("Login successful");
        console.log(response.data);
      } else {
        console.log(`Server responded with status code ${response.status}`);
      }
    } catch (error) {
      if (error.response) {
        console.log(`Server responded with status code ${error.response.status}`);
      } else if (error.request) {
        console.log("No response received from server");
      } else {
        console.log(`Error: ${error.message}`);
      }
    }
  };

  return (
    <div className="container">
      <div className="container-form">
        <form onSubmit={handleLogin}>
          <h1>Login</h1>
          <p>Please fill the input below here.</p>

          <div className="inputBox">
            <FiMail className="mail" />
            <input
              type="email"
              name="email"
              id="email"
              onChange={handleChange}
              placeholder="Email"
            />
          </div>

          <div className="inputBox">
            <RiLockPasswordLine className="password" />
            <input
              type="password"
              name="password"
              id="password"
              onChange={handleChange}
              placeholder="Password"
            />
          </div>

          <div className="divBtn">
            <button className="loginBtn" onClick={handleLogin}>
              LOGIN
            </button>
          </div>
          <div className='dont'>
                <p>Don't have an account? <Link to="/signup"><span>Sign up</span></Link></p>
            </div>
        </form>
      </div>
    </div>
  );
};

export default Login;